## Estructura del proyecto

#### Instalación de parcel-bundler
- [x] Crear el archivo `package.json` con la información básica del proyecto.
~~~
npm init -y
~~~

- [x] `parcel-bundler`, empaqueta todos los archivos, parcel por defecto reconoce código JSX (react).
~~~
npm i -D parcel-bundler
~~~

- [x] `ReactDOM` para aplicaciones web.
~~~
npm i react react-dom
~~~

- [x] Revisar el el archivo `package.json`, debe ser parecido a esto:
~~~
{
  "name": "icecream-app",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "parcel-bundler": "^1.12.5"
  }
}
~~~

- [x] Modificar algunos alias dentro del script para poder ejecutar nuestras propias instrucciones personaizadas:
~~~
{
  "name": "icecream-app",
  "version": "1.0.0",
  "description": "",
  "main": "./src/js/index.js",
  "scripts": {
    "start": "parcel ./src/index.html",
    "build": "parcel build ./src/index.html --public-url ./"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "parcel-bundler": "^1.12.5"
  },
  "dependencies": {
    "react": "^17.0.2",
    "react-dom": "^17.0.2"
  }
}

~~~

#### Instalaciones de paquetería en `react`
~~~
npm install axios --save
npm install express body-parser --save
npm install node-sass --save
npm install prettier
npm install prop-types
npm install react-helmet --save
npm install react-router-dom --save
npm install react-scripts --save
npm install uniqid --save
~~~
`grep -l -r -i "style" src/` buscar de forma recursiva en el directorio